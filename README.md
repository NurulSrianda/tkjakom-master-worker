# tkjakom-master-worker

Tugas Socket Programming Maka Kuliah Jaringan Komputer

Anggota:
- Amalia Hanisafitri
- Nurul Srianda Putri
- Shafiya Ayu Adzhani

To run this program, open two terminal:
- run `python worker.py <PORT>`
- run `python master.py <PORT>`

Command that can be done from master:
- `close`

    Master send close connection to worker
- `status`

    Check availability status of worker
- `which_job <JOB_NUMBER>`

    Ask if worker does JOB_NUMBER or not
- `what_job`

    Check latest job that has been done
- `where`

    Check which worker that do the job

- `1 <NUMBERS SEPARATED BY SPACE>`

   Do sum of numbers

- `2 <NUMBERS SEPARATED BY SPACE>`

   Do bubble sort of numbers

- `3 <NUMBER>`

    Calculate fibonacci of NUMBER