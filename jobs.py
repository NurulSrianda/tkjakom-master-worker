import time

__all__ = ['Job', 'SuicideJob', 'SimpleJob']


class Job(object):
    "Interface for a Job object."
    def __init__(self):
        pass

    def run(self):
        "The actual task for the job should be implemented here."
        pass


class SuicideJob(Job):
    "A worker receiving this job will commit suicide."
    def run(self, **kw):
        raise TerminationNotice()

class SimpleJob(Job):
    """
    Given a `result` queue, a `method` pointer, and an `args` dictionary or
    list, the method will execute r = method(*args) or r = method(**args),
    depending on args' type, and perform result.put(r).
    """
    def __init__(self, result, method, args=[]):
        self.result = result
        self.method = method
        self.args = args

    def job_level_one(self, *args):
        '''
        Return list of numeric value in args
        '''
        print("Executing job level one...")
        number= []
        for d in args:   
            try:
                number.append(float(d))
            except:
                print(d, "is not a number")
        return sum(number)

    def job_level_two(self, *args):
        '''
        Return sorted numeric value in args.
        Sorting is done using bubble sort algorithm
        '''
        print("Executing job level two...")
        number_list = [i for i in args if isinstance(i, float)]
        n = len(number_list)

        for i in range(n-1): 
            for j in range(0, n-i-1): 
                if number_list[j] > number_list[j+1] : 
                    number_list[j], number_list[j+1] = number_list[j+1], number_list[j]
        return number_list

    def job_level_three(self, *args):
        '''
        Return the nth fibonaci numbers.
        n = all number in args
        '''
        print("Executing job level three...")
        result = [fibonacci(i) for i in args if isinstance(i, int)]
        # print("Sleep for 1 minute")
        # time.sleep(60)
        return result

    def run(self):
        if isinstance(self.args, list) or isinstance(self.args, tuple):
            r = self.method(*self.args)
        elif isinstance(self.args, dict):
            r = self.method(**self.args)
        self._return(r)

    def _return(self, r):
        "Handle return value by appending to the ``self.result`` queue."
        self.result.put(r)

def fibonacci(anumber):
        if anumber<=0:
            print("Incorrect input")
        # Base case
        elif anumber==1:
            return 0
        elif anumber==2:
            return 1
        # recursive case
        else:
            return fibonacci(anumber-1)+fibonacci(anumber-2)