"""Example socket client."""
import socket
import json
import sys

def main():
    """Test Socket Master."""
    # create an INET, STREAMing socket, this is TCP
    port = int(sys.argv[1])
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect to the server
    sock.connect(("localhost", port))

    # set socket to be setblocking
    sock.setblocking(0)

    # properties to check
    booleanInput = True
    status = "available"
    job_doing = 0
    while booleanInput:
        try:
            data = sock.recv(1024)
            if data:
                result = helper(data)
                print("Hasil dari job " + str(result["result"]))
                status = "available"
        except:
            pass
        input_user = str(input("Enter your command: "))
        # close connection
        if input_user == "close":
            message = json.dumps({"command": 0, "argument": ""})
            sock.sendall(message.encode('utf-8'))
            sock.close()
            break
        # check status of worker
        elif input_user == "status":
            print("Worker is " + str(status))

        # check if worker does the job that has been asked
        elif input_user.split()[0] == "which_job":
            if int(job_doing) == int(input_user.split()[1]):
                print("Worker is doing / has done job " + str(job_doing))
            else:
                print("Worker isn't doing job " + str(input_user.split()[1]))
        
        # check the latest job that has been done by worker
        elif input_user.split()[0] == "what_job":
            print("Worker is doing job " + str(job_doing))

        # check which worker that do the job
        elif input_user == "where":
            print(1)

        # do job 1
        elif input_user.split()[0] == "1":
            job_doing = 1
            pass_input = []
            input_list = input_user.split()
            for i in range(1, len(input_list)):
                pass_input.append(int(input_list[i]))
            message = json.dumps({"command": 1, "argument": pass_input})
            sock.sendall(message.encode('utf-8'))
        
        # do job 2
        elif input_user.split()[0] == "2":
            job_doing = 2
            pass_input = []
            input_list = input_user.split()
            for i in range(1, len(input_list)):
                pass_input.append(int(input_list[i]))
            message = json.dumps({"command": 2, "argument": pass_input})
            sock.sendall(message.encode('utf-8'))
        
        # do job 3
        elif input_user.split()[0] == "3":
            job_doing = 3
            pass_input = []
            input_list = input_user.split()
            for i in range(1, len(input_list)):
                pass_input.append(int(input_list[i]))
            message = json.dumps({"command": 3, "argument": pass_input[0]})
            sock.sendall(message.encode('utf-8'))
        else:
            break
        sock.settimeout(2)
        if input_user != "close" and input_user != "status" and input_user.split()[0] != "what_job" and input_user.split()[0] != "which_job" and input_user != "where":
            try:
                print("Job is starting")
                data = sock.recv(1024)
                if data:
                    result = helper(data)
                    print("Hasil dari job " + str(result["result"]))
                    status = "available"
            except:
                status = "busy"
                print("Worker is busy")

def helper(message_bytes):
    message_str = message_bytes.decode("utf-8")
    message_dict = json.loads(message_str)
    return message_dict

if __name__ == "__main__":
    main()
