import time

__all__ = ['Job', 'SimpleJob']

class Job(object):
    "Interface for a Job object."
    def __init__(self):
        pass

    def run(self):
        "The actual task for the job should be implemented here."
        pass

class SimpleJob(Job):
    """
    Given a `result` queue, a `method` pointer, and an `args` dictionary or
    list, the method will execute r = method(*args) or r = method(**args),
    depending on args' type, and perform result.put(r).
    """
    def __init__(self, result):

        self.result = result

    def job_level_one(self, list):
        '''
        Return list of numeric value in args
        '''
        print("Executing job level one...")
        number= []
        for d in list:   
            try:
                number.append(int(d))
            except ValueError:
                print('Invalid input, try again')
        return sum(number)

    def job_level_two(self, list):
        '''
        Return sorted numeric value in args.
        Sorting is done using bubble sort algorithm
        '''
        print("Executing job level two...")
        number_list = list
        n = len(number_list)

        for i in range(n-1): 
            for j in range(0, n-i-1): 
                if number_list[j] > number_list[j+1] : 
                    number_list[j], number_list[j+1] = number_list[j+1], number_list[j]
        return number_list

    def job_level_three(self, *args):
        '''
        Return the nth fibonaci numbers.
        n = all number in args
        '''
        print("Executing job level three...")
        result = [fibonacci(i) for i in args if isinstance(i, int)]
        return result

    # def run(self):
    #     if isinstance(self.args, list) or isinstance(self.args, tuple):
    #         r = self.method(*self.args)
    #     elif isinstance(self.args, dict):
    #         r = self.method(**self.args)
    #     self._return(r)

    # def _return(self, r):
    #     "Handle return value by appending to the ``self.result`` queue."
    #     self.result.put(r)

def fibonacci(n):
    if n<=0:
        print("Incorrect input")
    # Base case
    elif n==1:
        return 0
    elif n==2:
        return 1
    # recursive case
    else:
        return fibonacci(n-1)+fibonacci(n-2)